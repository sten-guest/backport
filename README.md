Unlike previous work such as backport-dsc (Bug #660208) and Russ
Allbery's
[backport](https://www.eyrie.org/~eagle/software/scripts/backport.html),
this script currently specialises in building from a source package
maintained in git rather than from a dsc.  It does not automate the
work of backporting, and only guards issues encountered when building
formal backports, such as the following:

  * building a backport on the wrong branch (lintian catches bad
    changelog entries)
  * building a backport in the wrong chroot/schroot/LXC or with the wrong
    pbuilder tarball
  * forgetting to generate the correct foo.changes file, or generating
    one against the wrong version
  * not realising that a backport will need to pass through NEW


TODO:

Support building a formal backport from a dsc; this is useful for
anyone who prefers the traditional source package workflow, and is
also useful for someone who sponsors backports from mentors.
  * Is this really needed when dget URL.dsc will unpack the source package?
  * Alternatively, support building from unpacked source packages rather than
    dscs.
